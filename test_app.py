import unittest
from app import app

class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_home(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

        # Decodifica los datos de la respuesta de bytes a una cadena
        response_text = response.data.decode('utf-8')

        expected_message = "¡Bienvenido a la Aplicación! 🎉🎈"
        self.assertIn(expected_message, response_text)

if __name__ == '__main__':
    unittest.main()